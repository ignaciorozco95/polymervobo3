import { setDocumentCustomStyles } from '@cells-components/cells-lit-helpers';
import { css } from 'lit-element';

setDocumentCustomStyles(css`
  body {
    margin: 0;
    font-family: var(--cells-fontDefault, 'Benton Sans', sans-serif);
  }

  @media (max-width: 768px) {
    *,
    *:before,
    *:after {
      -webkit-tap-highlight-color: transparent;
    }
  }
`);
